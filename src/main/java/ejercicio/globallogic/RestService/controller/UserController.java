package ejercicio.globallogic.RestService.controller;

import javax.validation.Valid;



import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.Errors;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import ejercicio.globallogic.RestService.controller.helpers.HelperCrudInterface;
import ejercicio.globallogic.RestService.controller.helpers.Validators;
import ejercicio.globallogic.RestService.payload.request.UserRequest;
import ejercicio.globallogic.RestService.payload.response.ApiResponse;
import ejercicio.globallogic.RestService.services.impl.UserServiceImpl;

@RestController
@RequestMapping("/api/users")
public class UserController implements HelperCrudInterface<UserRequest> {

    private static final Logger logger = LoggerFactory.getLogger(UserController.class);


    @Autowired
    UserServiceImpl userService;


    @Override
    @PostMapping("/auth/signin")
    public ResponseEntity<?> auth(@Valid @RequestBody UserRequest value, Errors error) {

        return userService.authenticate(value);
    }

    @Override
    @PostMapping("/auth/register")
    public ResponseEntity<?> register(@Valid @RequestBody UserRequest value, Errors error) {

        System.out.println(value.getPhones());
    
        if (!Validators.isValidEmail(value.getEmail())) {
            logger.info("Email Incorrecto");
            return new ResponseEntity(new ApiResponse("Formato incorrecto de correo."), HttpStatus.OK);
        }

        if (!Validators.validatePassword(value.getPassword())) {
            logger.info("Password sin formato");
            return new ResponseEntity(new ApiResponse("Formato incorrecto, debe seguir la sieguiente regla: (Una Mayúscula, letras minúsculas, y dos números)"), HttpStatus.OK);
        }

        String mensaje = "";
        if (error.hasErrors()) {
            for (ObjectError validator : error.getAllErrors()) {
                mensaje = mensaje+" "+validator.getDefaultMessage()+", ";
                logger.info(validator.getDefaultMessage());
            }

            return new ResponseEntity(new ApiResponse(mensaje), HttpStatus.OK);
        }


        

        return userService.register(value);
    }



     

}