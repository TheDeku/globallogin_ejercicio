package ejercicio.globallogic.RestService.controller.helpers;

import org.springframework.http.ResponseEntity;
import org.springframework.validation.Errors;

public interface HelperCrudInterface<T> {
        

    public ResponseEntity<?> register( T value,Errors error);

    public ResponseEntity<?> auth( T value, Errors error);

}