package ejercicio.globallogic.RestService.jwt;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import ejercicio.globallogic.RestService.models.User;
import ejercicio.globallogic.RestService.repository.UserRepository;

@Service
public class CustomUserDetailService implements UserDetailsService{


    @Autowired
    UserRepository userRepository;

    @Override
    @Transactional
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		User user = userRepository.findByName(username)
                .orElseThrow(() ->
                        new UsernameNotFoundException("El usuario no fue encontrado : " + username)
        );

        return JWTUser.create(user);
	}
    

        @Transactional
        public UserDetails loadUserById(Long id) {
                User user = userRepository.findById(id).orElseThrow(()-> new UsernameNotFoundException("No se encontro el ID del usuario")
                    );
    
            return JWTUser.create(user);
        }
}