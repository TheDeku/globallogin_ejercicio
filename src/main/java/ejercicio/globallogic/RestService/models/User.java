package ejercicio.globallogic.RestService.models;

import javax.persistence.Entity;
import javax.persistence.Table;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;

import org.hibernate.annotations.NaturalId;

import ejercicio.globallogic.RestService.models.audits.DateAudit;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.*;



@Entity
@Table(name = "usuarios")
public class User extends DateAudit{

    private static final long serialVersionUID = 1L;
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotEmpty
    @Size(max = 50)
    private String name;

    @NotEmpty
    @Email
    @NaturalId
    @Size(max=50)
    private String email;

    @NotEmpty
    @Size(max=60)
    private String password;


    @Column(name = "isactive")
    private Boolean active;



    /**
     * Generate a user
     */
    public User() {
        
    }

    

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the email
     */
    public String getEmail() {
        return email;
    }

    /**
     * @param email the email to set
     */
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     * @return the password
     */
    public String getPassword() {
        return password;
    }

    /**
     * @param password the password to set
     */
    public void setPassword(String password) {
        this.password = password;
    }


    

    /**
     * @return Long return the id
     */
    public Long getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * @param name
     * @param email
     * @param password
     * @param userInfo
     */
    public User(@NotEmpty @Size(max = 50) String name, @NotEmpty @Email @Size(max = 50) String email,
            @NotEmpty @Size(max = 15) String password,boolean active) {
        this.name = name;
        this.email = email;
        this.password = password;
        this.active = active;


    }


    /**
     * @return Boolean return the active
     */
    public Boolean isActive() {
        return active;
    }

    /**
     * @param active the active to set
     */
    public void setActive(Boolean active) {
        this.active = active;
    }

    /**
     * @return the active
     */
    public Boolean getActive() {
        return active;
    }

  

}