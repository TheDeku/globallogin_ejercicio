package ejercicio.globallogic.RestService.payload.request;

import java.io.Serializable;

import javax.validation.constraints.Size;

public class UserInfoRequest implements Serializable {
    
    
    /**
     *
     */
    private static final long serialVersionUID = 1L;

    @Size(max = 8)
    private String number;

    @Size(max = 3,min=1)
    private String cityCode;

    @Size(max = 3,min=1)
    private String countryCode;

    /**
     * 
     */
    public UserInfoRequest() {
    }

   
    
    


    /**
     * @return String return the number
     */
    public String getNumber() {
        return number;
    }

    /**
     * @param number the number to set
     */
    public void setNumber(String number) {
        this.number = number;
    }

    /**
     * @return String return the cityCode
     */
    public String getCityCode() {
        return cityCode;
    }

    /**
     * @param cityCode the cityCode to set
     */
    public void setCityCode(String cityCode) {
        this.cityCode = cityCode;
    }

    /**
     * @return String return the countryCode
     */
    public String getCountryCode() {
        return countryCode;
    }

    /**
     * @param countryCode the countryCode to set
     */
    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

}