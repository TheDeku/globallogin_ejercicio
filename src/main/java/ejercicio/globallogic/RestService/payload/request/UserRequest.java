package ejercicio.globallogic.RestService.payload.request;

import java.util.ArrayList;
import java.util.List;

import javax.validation.constraints.Email;
import javax.validation.constraints.Size;

public class UserRequest {
    
    @Size(max = 50)
    private String name;

    @Email
    @Size(max = 50)
    private String email;

    @Size(max = 50)
    private String password;

    private List<UserInfoRequest> phones;

    /**
     * 
     */
    public UserRequest() {
        phones = new ArrayList<UserInfoRequest>();
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the email
     */
    public String getEmail() {
        return email;
    }

    /**
     * @param email the email to set
     */
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     * @return the password
     */
    public String getPassword() {
        return password;
    }

    /**
     * @param password the password to set
     */
    public void setPassword(String password) {
        this.password = password;
    }

    /**
     * @return the phones
     */
    public List<UserInfoRequest> getPhones() {
        return phones;
    }

    /**
     * @param phones the phones to set
     */
    public void setPhones(List<UserInfoRequest> phones) {
        this.phones = phones;
    }

    

}