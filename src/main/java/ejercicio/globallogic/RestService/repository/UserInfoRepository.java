package ejercicio.globallogic.RestService.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import ejercicio.globallogic.RestService.models.UserInfo;

@Repository
public interface UserInfoRepository extends JpaRepository<UserInfo, Long> {
    
    Optional<UserInfo>  findByCountryCode(Integer countryCode);
    Optional<UserInfo> findByCityCode(Integer cityCode);
    Optional<UserInfo> findByNumber(Integer number);
}