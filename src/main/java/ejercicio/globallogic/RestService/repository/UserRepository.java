package ejercicio.globallogic.RestService.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import ejercicio.globallogic.RestService.models.User;

@Repository
public interface UserRepository extends JpaRepository<User,Long>{

    Optional<User>  findByEmail(String email);
    Optional<User> findByNameOrEmail(String username, String email);
    List<User> findByIdIn(List<Long> userIds);
    Optional<User> findByName(String name);
    Boolean existsByName(String name);
    Boolean existsByEmail(String email);
    
}