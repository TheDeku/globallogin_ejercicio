package ejercicio.globallogic.RestService.services;

import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Repository;

import ejercicio.globallogic.RestService.payload.request.UserInfoRequest;


@Repository
public interface UserInfoService {

    ResponseEntity<?> authenticate(UserInfoRequest request);

    ResponseEntity<?> register(UserInfoRequest request);
}