package ejercicio.globallogic.RestService.services.impl;


import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import ejercicio.globallogic.RestService.exception.AppException;
import ejercicio.globallogic.RestService.jwt.JwtTokenProvider;

import ejercicio.globallogic.RestService.models.User;
import ejercicio.globallogic.RestService.models.UserInfo;

import ejercicio.globallogic.RestService.payload.request.UserInfoRequest;
import ejercicio.globallogic.RestService.payload.request.UserRequest;
import ejercicio.globallogic.RestService.payload.response.ApiResponse;
import ejercicio.globallogic.RestService.payload.response.JwtAuthResponse;
import ejercicio.globallogic.RestService.payload.response.UserResponse;

import ejercicio.globallogic.RestService.repository.UserInfoRepository;
import ejercicio.globallogic.RestService.repository.UserRepository;
import ejercicio.globallogic.RestService.services.UserService;


@Service
public class UserServiceImpl implements UserService {

    @Autowired
    AuthenticationManager authenticationManager;

    @Autowired
    UserRepository userRepository;

    @Autowired
    UserInfoRepository userInfoRepository;

    @Autowired
    PasswordEncoder passwordEncoder;

    @Autowired
    JwtTokenProvider tokenProvider;



    @Override
    public ResponseEntity<?> authenticate(UserRequest request) {
        Authentication authentication = authenticationManager
                .authenticate(new UsernamePasswordAuthenticationToken(request.getName(), request.getPassword()));

        SecurityContextHolder.getContext().setAuthentication(authentication);

        String jwt = tokenProvider.generateToken(authentication);
        return ResponseEntity.ok(new JwtAuthResponse(jwt));
    }

    @Override
    public ResponseEntity<?> register(UserRequest request) {


        UserResponse response;
        try {


            if (userRepository.existsByEmail(request.getEmail())) {
                return new ResponseEntity(new ApiResponse("Correo electronico ya registrado"), HttpStatus.OK);
            }


            User user = new User(request.getName(), request.getEmail(), passwordEncoder.encode(request.getPassword()), true);

            try {
                System.out.println(new ObjectMapper().writeValueAsString(user));
            } catch (JsonProcessingException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
           
            userRepository.save(user);

            for (UserInfoRequest phone : request.getPhones()) {
           
                UserInfo info = new UserInfo();
                info.setNumber(phone.getNumber());
                info.setCountryCode(phone.getCountryCode());
                info.setCityCode(phone.getCityCode());
                info.setUser(user);
                userInfoRepository.save(info);
                
            }

            User usuario = userRepository.findByName(user.getName()).orElseThrow(()->new AppException("No se encontro ningun usuario"));
  
            Authentication authentication = authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(usuario.getName(), request.getPassword()));
    
            SecurityContextHolder.getContext().setAuthentication(authentication);
            String tokenType = "Bearer";
            String jwt = tokenType +" "+ tokenProvider.generateToken(authentication);

            response = new UserResponse(usuario, jwt);
        } catch (Exception e) {
            return new ResponseEntity(new ApiResponse("Hubo un error en procesar el registro: "+e), HttpStatus.CONFLICT);
        }

        return new ResponseEntity(response, HttpStatus.OK);
    }


}