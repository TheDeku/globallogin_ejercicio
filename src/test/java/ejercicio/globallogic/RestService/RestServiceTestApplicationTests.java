package ejercicio.globallogic.RestService;

import java.util.ArrayList;
import java.util.List;

import org.junit.*;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import ejercicio.globallogic.RestService.payload.request.UserInfoRequest;
import ejercicio.globallogic.RestService.payload.request.UserRequest;
import ejercicio.globallogic.RestService.services.impl.UserServiceImpl;

@RunWith(SpringRunner.class)
@SpringBootTest
@ContextConfiguration(classes = RestServiceTestApplication.class)
class RestServiceTestApplicationTests {

	@Autowired
	private UserServiceImpl userServiceImpl;

	@Before
	public void before() {
		userServiceImpl = new UserServiceImpl();
	}

	@Test
	public void test() throws Exception {
		UserRequest user = new UserRequest();
		user.setEmail("Correo@Correo.cl");
		user.setName("Nombre");
		user.setPassword("Password1234");

		List<UserInfoRequest> list = new ArrayList<>();
		for (int i = 0; i < 2; i++) {
			UserInfoRequest info = new UserInfoRequest();
			info.setCityCode("22");
			info.setCountryCode("123");
			info.setNumber("12345678");
			list.add(info);
		}
		user.setPhones(list);

		System.out.println(userServiceImpl.register(user).getStatusCode());
	}

}
